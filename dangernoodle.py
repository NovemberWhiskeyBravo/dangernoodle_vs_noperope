
class Snake(object):
    """docstring for Snake."""

    def __init__(self, snakeID,name ,colour):
        print("created a snek")
        self.snakeID = snakeID
        self.name = name
        self.colour = colour
        self.length = 10
        self.direction = 'up'
        self.x = 0
        self.y = 400
        self.body = []

        for i in range(self.length):
            self.body.append([0,0])

    def moveBodyParts(self):
        tempBody = []
        #print("I LIKE TO MOVE IT MOVE IT!")
        for i in range(len(self.body)):
            if i == 0:
                tempBody.append([self.x,self.y])
            else:
                tempBody.append(self.body[i-1])
        self.body = tempBody

    def addBodyPart(self):
        self.body.append(self.body[len(self.body)-1])

    def removeBodyParts(self, numberOfRemovingParts):
        self.body = self.body[:-int(numberOfRemovingParts)]

    def checkBodyParts(self):
        if self.body[0][0] <= -1:
            self.direction = "left"
        elif self.body[0][0] >= 701:
            self.direction = "right"

        if self.body[0][1] <= -1:
            self.direction = "down"
        elif self.body[0][1] >= 501:
            self.direction = "up"

    #getters and setters
    def setDirection(self, direction):
        self.direction = direction

    def getColour(self):
        return self.colour

    def getDirection(self):
        return self.direction

    def getBody(self):
        return self.body

    def setX(self, x):
        self.x = x

    def setY(self, y):
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getLength(self):
        return self.length

    def setLength(self, length):
        self.length = length
