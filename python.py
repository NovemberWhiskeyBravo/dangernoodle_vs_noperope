import pygame, random, sys, threading,time
from dangernoodle import Snake
from apple import Apple

pygame.init()

# Define some colors
BLACK = ( 0, 0, 0)
WHITE = ( 255, 255, 255)
GREEN = ( 0, 255, 0)
RED = ( 255, 0, 0)

playerColours = [RED,GREEN]

speed = 2

# Open a new window
size = (700, 500)
screen = pygame.display.set_mode(size)
pygame.display.set_caption('DangerNoodle VS NopeRope!')

directions = ["up","right","down","left"]
playerNames = ["DangerNoodle","NopeRope","Snek","Wuurm"]

players = []

# The loop will carry on until the user exit the game (e.g. clicks the close button).
carryOn = True

# The clock will be used to control how fast the screen updates
clock = pygame.time.Clock()

# Initialize the joysticks
pygame.joystick.init()

# Get count of joysticks
joystick_count = pygame.joystick.get_count()

# --------------- Init -----------------
print("starting gameInit")

apple = Apple(random.randint(1,700),random.randint(1,500))

for i in range(joystick_count):
    print("> Created a player")
    players.append(Snake(i,playerNames[i],playerColours[i]))

#creating a timed event
pygame.time.set_timer((pygame.USEREVENT+2),5000)

print("done with gameInit")

# -------- Main Program Loop -----------
while carryOn:
    # --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
              carryOn = False # Flag that we are done so we exit this loop

        #if event.type == (pygame.USEREVENT+2):
            #for player in players:
                #player.addBodyPart()

    # --- Game logic
    for i in range(joystick_count):
        joystick = pygame.joystick.Joystick(i)
        joystick.init()

        axes = joystick.get_numaxes()
        joystickID = joystick.get_id()

        for i in range( axes ):
            axis = int(round(joystick.get_axis(i)))
            if i == 0:
                #right
                if axis == -1:
                    players[joystickID].setDirection(directions[1])
                #left
                elif axis == 1:
                    players[joystickID].setDirection(directions[3])
            elif i == 1:
                #up
                if axis == -1:
                    players[joystickID].setDirection(directions[0])
                #down
                elif axis == 1:
                    players[joystickID].setDirection(directions[2])

    for player in players:

        if player.getDirection() == directions[0]:
            player.setY(player.getY()-speed)
        elif player.getDirection() == directions[1]:
            player.setX(player.getX()-speed)
        elif player.getDirection() == directions[2]:
            player.setY(player.getY()+speed)
        elif player.getDirection() == directions[3]:
            player.setX(player.getX()+speed)



    # --- Drawing code
    # First, clear the screen to white.
    screen.fill(BLACK)
    #The you can draw different shapes and lines or add text to your background stage.

    apple_image = pygame.draw.rect(screen, RED, [apple.getX(), apple.getY(), 15, 15],0)

    for player in players:
        player.moveBodyParts()
        player.checkBodyParts()

        for part in player.getBody():
            pygame.draw.rect(screen, player.getColour(), [part[0], part[1], 10, 10],0)

        player_head = pygame.draw.rect(screen, player.getColour(), [player.getX(), player.getY(), 10, 10],0)

        if player_head.colliderect(apple_image):
            for i in range(9):
                player.addBodyPart()

            apple.setX(random.randint(1,700))
            apple.setY(random.randint(1,500))

    #pygame.draw.rect(screen, GREEN, [players[1].getX(), players[1].getY(), 10, 10],0)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
    # --- Limit to 60 frames per second
    clock.tick(60)


#Once we have exited the main program loop we can stop the game engine:
pygame.quit()
